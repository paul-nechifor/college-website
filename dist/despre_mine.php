<?php
require 'include/antet.php';
afiseazaAntet('Despre Mine', '', '');
?>

<h2 id="t_despre_mine">Despre mine</h2>

<img src="imagini/portret.jpg" alt="Paul Nechifor" style="float:right; border:1px solid #1b1010; margin: 5px" />
<p><span class="M"><span>M</span></span>&#259; numesc Paul R&#259;zvan Nechifor &#351;i am 19 ani. Sunt student la Facultatea de Informatica Ia&#351;i (FII) de la Universitatea Alexandru Ioan Cuza. Scuza&#355;i-m&#259; dac&#259; textul pe care-l scriu nu are niciun sens &#351;i rost, dar dup&#259; ora 3 noaptea (&icirc;n ziua &icirc;n care trebuie s&#259; termin neap&#259;rat saitul) degetele mele scriu singure (creierul fiind &icirc;n hibernare).</p>
<p>&Icirc;mi place s&#259; joc jocuri interesante care au o grafic&#259; c&acirc;t-de-c&acirc;t decent&#259; &#351;i au o poveste &icirc;n spatele ac&#355;inunii. Printre cele mai bune jocuri cred c&#259; sunt: <em>Max Payne</em> (1 &#351;i 2) pentru TPS, <em>GTA Vice City</em>, <em>GTA San Andreas</em> pentru genul action, <em>Warcraft III</em> pentru RTS (n-am jucat niciodat&#259; <em>Starcraft</em>), <em>The Elder Scrolls III: Morrowind</em> pentru RPG (&#351;i <em>Oblivion</em> e bun, dar nu se compar&#259; cu <em>Morrowind</em>),<em>Unreal Tournament</em> (2004 &#351;i 3) pentru FPS, <em>Spore</em> pentru God-game &#351;i cred c&#259; <em>King's Bounty: The Legend</em> pentru TBS. De&#351;i nu &icirc;mi placeau TBS-urile p&acirc;n&#259; acum, <em>King's Bounty</em> mi se pare interesant pentru c&#259; este combinat cu elemente de RPG ca &#351;i <em>Warcraft</em>, de&#351;i sunt jocuri de strategie.</p>
<p>Ascult foarte multe genuri de muzic&#259;, de o varia&#355;ie foarte mare. De la Death Metal la Black Metal, de la Gothic Metal la Funeral Doom Metal, de la Folk Metal la Symphonic Black Metal,... &#350;i multe altele cum ar fi Symphonic Metal, Viking Metal,  Melodic Death Metal, Blackened Death Metal, Thrash Metal, New Wave of British Heavy Metal, Gothic-influenced Death Metal, Black Funeral Doom Metal,... De toate genurile. A&#351; face o list&#259; cu trupe care-mi plac foarte mult, dar ar fi prea lung&#259; &#351;i ar dura mult s-o fac. Oricum, aproape de v&acirc;rf ar fi , nu neaparat &icirc;n ordinea asta, Bathory, Burzum, Mayhem, Marduk, Cannibal Corpse, Ulver, Haggard, Funeral, Theatre of Tragedy, Negur&#259; Bunget (cea mai tare trup&#259; rom&acirc;neasc&#259; de Black Metal sau orice alt gen), Cradle of Filth, Deicide &#351;i Dimmu Borgir.</p>
<?php
require 'include/subsol.php';
afiseazaSubsol();
?>